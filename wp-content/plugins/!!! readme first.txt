
First of all I would like to thank you for your purchase!
Visual Composer for WordPress is very powerful tool and soon you will wonder how did you live without it… But first, take a moment to read this file. 
   

/* Documentation
---------------------------------------------------------- */
   
   Using Visual Composer is very straight forward process, most accurate documentation is always available in the WPBakery Knowledge Base site:
   http://kb.wpbakery.com/index.php?title=Visual_Composer
   
   

/* js_composer.zip
---------------------------------------------------------- */
   
   js_composer.zip is plugin itself and it should be uploaded to your site, according to WordPress plugin installation rules.
   http://codex.wordpress.org/Managing_Plugins#Installing_Plugins
