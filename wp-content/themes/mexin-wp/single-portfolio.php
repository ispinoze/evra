<?php
get_header();
?>
<br/>
<section class="container page-content" >


<article class="sixteen columns" >
<?php 
if(have_posts()): while(have_posts()): the_post();

echo the_content();

endwhile;
endif;

?>
</article>

</section>
  <?php get_footer(); ?>